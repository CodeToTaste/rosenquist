﻿=== WooCommerce Shipping Label ===

Contributors: WooForce, elvinwf
Donate link: 
Tags: Print Shipping Label, Print Address Label,Print Invoice,Print Packing List, Packing Slip, Shipping,Labels, Invoice, Packing Lists, WooCommerce, WordPress, Delivery Note

Requires at least: 3.0.1
Tested up to: 4.7
Stable tag: 2.0.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Generate & Print Shipping Label / Address Label within WooCommerce Orders. Print WooCommerce PDF Label.

== Description ==

= Introduction =
Tired of copying and pasting order details just to create a label? Here is the solution! Use 'Shipping Label Plugin for WooCommerce’ to generate address label for your packages.
WooForce WooCommerce Shipping Label Plugin is a Wordpress / WooCommerce extension that helps you to generate and print the shipping label from within WooCommerce orders. Apart from destination address and origin address, you can also add company's logo, company name, return policy and custom footer on the shipment label. The label printed also contains the order number, weight of the package and date of the shipment label printing.
With this plugin, you can generate shipping label when you are using Free Shipping, Flatrate Shipping and Shipping Pro.

= Features =

* <strong>Generate Shipping/Address Label:</strong> Generate and print shipping label from within Woocommerce.From WordPress admin area, you can generate label for single order from order details page. 
* <strong>Configure Base Address (Shipping From)</strong>: You can set the base address of the store which will be used in the label as the 'shipping from' address.
* <strong>Customize Label with Store Name and Company Logo</strong>: You can also set the store name which can be displayed on the label. There is also an option to add the logo to the label.
* <strong>Print Additional Info</strong>: Additionally, the following details can be configured from settings page so that it is added on the banner.
<br>Return Policy, Conditions, etc. : With this field, you can add Return Policy, condition, etc. on Label with the limit of 75 characters.
<br>Custom footer: With this field, you can add the required footer on Label with the limit of 75 characters.
* <strong>Configure Label Size</strong>: With this field, you can set the size of Label. Full Page or 4 x 6 inches.
* <strong>Generate Shipping Label in PDF format.</strong>
* <strong>Preview before printing</strong>: This feature allows you to view what a printed Label would look like on the screen before printing a hard copy.

<blockquote>

= Premium version Features =
<ul>
<li> Print Invoice, Packing List, Delivery Note & Label from within WooCommerce Order Page. </li>
<li> Various Packing Options Box Packing, Weight Based Packing, Pack Items Individually and Single Package</li>
<li> Bulk printing Invoice, Packing List, Delivery Note & Label from Order List. </li>
<li> Add your company logo as part of Label, Invoice, Packing List and Delivery Note.</li>
<li> Customize Invoice Number by prefix,postfix and padding feature.</li>
<li> Attach invoice, packing slip and Delivery Note with Order Completion Email.</li>
<li> Add additional checkout fields SSN and VAT and display it on Invoice.</li>
<li> Add additional columns item image and price with Packing Slip and Delivery Note.</li>
<li> Options to add customized return policy and footer for Label, Packing Slip, Invoice and Delivery Note.</li>
<li> Configure Label Size: With this field, you can set the size of Label. Full Page or 4 x 6 inches.</li>
<li> Choice for the option Preview before printing.</li>
<li> WPML compatible. FR(French), DE(German) and DK(Danish) supported out of the box.</li>
<li> Third Party Tracking Number and Shipment Date supported through code snippet.</li>
<li> Timely compatibility updates and bug fixes.</li>
<li> Premium support!</li>
</ul>

For complete list of features and details, please visit <a rel="nofollow" href="http://www.xadapter.com/product/print-invoices-packing-list-labels-for-woocommerce/">Print Invoice, Packing List, Delivery Note & Label for WooCommerce</a>
</blockquote>

= About WooForce =

WooForce creates quality WordPress/WooCommerce plug-ins that are easy to use and customize. We are proud to have thousands of customers actively using our plugins across the globe.

== Installation ==

1. Upload the plugin folder to the ‘/wp-content/plugins/’ directory.
2. Activate the plugin through the ‘Plugins’ menu in WordPress.
3. That's it – you can now configure the plugin.

== Frequently Asked Questions ==

= Is the plugin configuration complicated? =
The plugin is very easy to configure. We have a step by step tutorial on setting up this plugin. Our Help Desk also has extensive documentation which includes FAQs, Troubleshooting Guide, Knowledge Base and Code snippets. Please [visit](http://www.xadapter.com/support/forum/print-invoices-packing-list-labels-for-woocommerce/) if you need any help. 

== Screenshots ==

1. Shipping Label
2. Plugin Configuration Screen
3. Admin Orders Screen
4. Individual Order Details Screen

== Changelog ==

= 2.0.4 =
 * Added option to enable or disable Contact Number in shipping label.
 * Supports custom logo to accept any size image.
 
= 2.0.3 =
 * Compatibility with WooCommerce version less than 2.5.0.
 
= 2.0.2 =
 * Removed blank line created by empty Address Line 2.
 * Compatibility with PHP version less than 5.4.

= 2.0 =
 * Enhanced Label Format.
 * Print PDF Shipping Label.
 * Major under the hood improvements.
 
= 1.0 =
 * Shipping Label Printing.
 
== Upgrade Notice ==

= 2.0.4 =
 * Added option to enable or disable Contact Number in shipping label.
 * Supports custom logo to accept any size image.
 
= 2.0.3 =
 * Compatibility with WooCommerce version less than 2.5.0.
 
= 2.0.2 =
 * Removed blank line created by empty Address Line 2.
 * Compatibility with PHP version less than 5.4.

= 2.0 =
 * Enhanced Label Format.
 * Print PDF Shipping Label.
 * Major under the hood improvements.
 
= 1.0.0 =
 * Shipping Label Printing.